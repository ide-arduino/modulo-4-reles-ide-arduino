//Modulo 4 Relé con ESP32

#define RELAY_ON 0
#define RELAY_OFF 1

void setup() {
  //Serial.begin(115200);
  
digitalWrite (2, RELAY_OFF);
digitalWrite (4, RELAY_OFF);
digitalWrite (12, RELAY_OFF);
digitalWrite (14, RELAY_OFF);

pinMode (2, OUTPUT);       //Pin D2
pinMode (4, OUTPUT);       //Pin D4
pinMode (12, OUTPUT);      //Pin D12
pinMode (14, OUTPUT);      //Pin D14

}

void loop() {
  // put your main code here, to run repeatedly:
  // Prueba de encendido Relé IN1 y IN2

digitalWrite (2, RELAY_ON);
digitalWrite (4, RELAY_ON);
delay (3000);
digitalWrite (2, RELAY_OFF);
delay (3000);
digitalWrite (4, RELAY_OFF);
delay (3000);


}
